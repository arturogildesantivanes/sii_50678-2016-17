#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <iostream>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fstream>
#include <math.h>

#include "glut.h"
#define MAX 100

using namespace std;

int main()
{
	mkfifo ("/tmp/tuberia_logger",0777);
	int fd_loggermundo = open("/tmp/tuberia_logger",O_RDONLY);
	while(1)
	{
		char cadena[MAX];
		read(fd_loggermundo,cadena,sizeof(cadena));
		printf("%s\n",cadena);	

	}
	close(fd_loggermundo);
	unlink("/tmp/tuberia_logger");
}
